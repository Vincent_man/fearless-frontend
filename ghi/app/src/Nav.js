import React from 'react';
import { NavLink } from 'react-router-dom';


function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference GO!</a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <React.Fragment>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="main"> Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/locations/new">New location</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/attendees/new">Attend Conference</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/attendees">Attendee list</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" aria-current="page" to="/presentations/new">New Presentation</NavLink>
                        </li>
                    </React.Fragment>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Nav;
